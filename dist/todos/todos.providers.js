"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.todosProviders = void 0;
const todos_entity_1 = require("./todos.entity");
exports.todosProviders = [
    {
        provide: 'TODOS_REPOSITORY',
        useValue: todos_entity_1.Todos,
    },
];
//# sourceMappingURL=todos.providers.js.map