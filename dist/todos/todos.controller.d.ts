import { TodosService } from './todos.service';
import { TodoDto, UpdateDto } from './dto';
export declare class TodosController {
    private todos;
    constructor(todos: TodosService);
    create(todo: TodoDto): Promise<{
        data: import("./todos.entity").Todos;
        message: import("./todos.service").Messages;
    }>;
    findAll(): Promise<{
        data: import("./todos.entity").Todos[];
        message: import("./todos.service").Messages;
    }>;
    update(todo: UpdateDto, id: number): Promise<{
        message: import("./todos.service").Messages;
    }>;
    delete(id: number): Promise<{
        message: import("./todos.service").Messages;
    }>;
}
