import { Model } from 'sequelize-typescript';
export declare class Todos extends Model {
    id: number;
    isDone: boolean;
    content: string;
}
