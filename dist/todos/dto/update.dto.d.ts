export declare class UpdateDto {
    content: string;
    isDone: boolean;
}
