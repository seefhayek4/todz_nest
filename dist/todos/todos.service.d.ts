import { TodoDto, UpdateDto } from './dto';
import { Todos } from './todos.entity';
export declare enum Messages {
    SUCCESS = "Todos received successfully",
    DELETE = "Todo deleted successfully",
    UPDATE = "Todo updated successfully",
    CREATE = "Todo created successfully",
    NOT_FOUND = "Todo not found"
}
export declare class TodosService {
    private Todos_Repository;
    constructor(Todos_Repository: typeof Todos);
    create(todo: TodoDto): Promise<{
        data: Todos;
        message: Messages;
    }>;
    findAll(): Promise<{
        data: Todos[];
        message: Messages;
    }>;
    update(id: number, todo: UpdateDto): Promise<{
        message: Messages;
    }>;
    delete(id: number): Promise<{
        message: Messages;
    }>;
}
