import { Todos } from './todos.entity';
export declare const todosProviders: {
    provide: string;
    useValue: typeof Todos;
}[];
