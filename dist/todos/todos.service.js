"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TodosService = exports.Messages = void 0;
const common_1 = require("@nestjs/common");
var Messages;
(function (Messages) {
    Messages["SUCCESS"] = "Todos received successfully";
    Messages["DELETE"] = "Todo deleted successfully";
    Messages["UPDATE"] = "Todo updated successfully";
    Messages["CREATE"] = "Todo created successfully";
    Messages["NOT_FOUND"] = "Todo not found";
})(Messages = exports.Messages || (exports.Messages = {}));
let TodosService = class TodosService {
    constructor(Todos_Repository) {
        this.Todos_Repository = Todos_Repository;
    }
    async create(todo) {
        const created = await this.Todos_Repository.create({ todo });
        return { data: created, message: Messages.CREATE };
    }
    async findAll() {
        const allTodos = await this.Todos_Repository.findAll();
        return { data: allTodos, message: Messages.SUCCESS };
    }
    async update(id, todo) {
        const updated = await this.Todos_Repository.update({ content: todo.content, isDone: todo.isDone }, { where: { id } });
        const message = updated[0] ? Messages.UPDATE : Messages.NOT_FOUND;
        return { message };
    }
    async delete(id) {
        const deleted = await this.Todos_Repository.destroy({ where: { id } });
        const message = deleted ? Messages.DELETE : Messages.NOT_FOUND;
        return { message };
    }
};
TodosService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)('TODOS_REPOSITORY')),
    __metadata("design:paramtypes", [Object])
], TodosService);
exports.TodosService = TodosService;
//# sourceMappingURL=todos.service.js.map