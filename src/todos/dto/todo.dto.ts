import { IsString, IsNotEmpty } from 'class-validator';

export class TodoDto {
  @IsString()
  @IsNotEmpty()
  content: string;
}
