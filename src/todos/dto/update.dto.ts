import { IsString, IsNotEmpty, IsBoolean } from 'class-validator';

export class UpdateDto {
  @IsString()
  @IsNotEmpty()
  content: string;

  @IsBoolean()
  isDone: boolean;
}
