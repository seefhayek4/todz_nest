import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
} from '@nestjs/common';
import { TodosService } from './todos.service';
import { TodoDto, UpdateDto } from './dto';

@Controller('todos')
export class TodosController {
  constructor(private todos: TodosService) {}
  @Post()
  create(@Body() todo: TodoDto) {
    return this.todos.create(todo);
  }

  @Get()
  findAll() {
    return this.todos.findAll();
  }

  @Put(':id')
  @HttpCode(HttpStatus.CREATED)
  update(@Body() todo: UpdateDto, @Param('id', new ParseIntPipe()) id: number) {
    return this.todos.update(id, todo);
  }

  @Delete(':id')
  async delete(@Param('id', new ParseIntPipe()) id: number) {
    return this.todos.delete(id);
  }
}
