import { Todos } from './todos.entity';

export const todosProviders = [
  {
    provide: 'TODOS_REPOSITORY',
    useValue: Todos,
  },
];
