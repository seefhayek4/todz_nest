import { Inject, Injectable } from '@nestjs/common';
import { TodoDto, UpdateDto } from './dto';
import { Todos } from './todos.entity';

export enum Messages {
  SUCCESS = 'Todos received successfully',
  DELETE = 'Todo deleted successfully',
  UPDATE = 'Todo updated successfully',
  CREATE = 'Todo created successfully',
  NOT_FOUND = 'Todo not found',
}

@Injectable()
export class TodosService {
  constructor(
    @Inject('TODOS_REPOSITORY')
    private Todos_Repository: typeof Todos,
  ) {}

  async create(todo: TodoDto) {
    const created = await this.Todos_Repository.create({ todo });

    return { data: created, message: Messages.CREATE };
  }

  async findAll() {
    const allTodos = await this.Todos_Repository.findAll();
    return { data: allTodos, message: Messages.SUCCESS };
  }

  async update(id: number, todo: UpdateDto) {
    const updated = await this.Todos_Repository.update(
      { content: todo.content, isDone: todo.isDone },
      { where: { id } },
    );

    const message = updated[0] ? Messages.UPDATE : Messages.NOT_FOUND;

    return { message };
  }

  async delete(id: number) {
    const deleted = await this.Todos_Repository.destroy({ where: { id } });

    const message = deleted ? Messages.DELETE : Messages.NOT_FOUND;
    return { message };
  }
}
