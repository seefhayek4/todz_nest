import { DataTypes } from 'sequelize';
import { Model, Column, Table } from 'sequelize-typescript';

@Table
export class Todos extends Model {
  @Column({ autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER })
  id: number;

  @Column({ defaultValue: false, type: DataTypes.BOOLEAN })
  isDone: boolean;

  @Column({ type: DataTypes.TEXT })
  content: string;
}
