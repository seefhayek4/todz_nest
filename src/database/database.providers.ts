import { Sequelize } from 'sequelize-typescript';
import { Todos } from '../todos/todos.entity';

export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async () => {
      const sequelize = new Sequelize({
        dialect: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: 'root',
        database: 'todo',
      });
      sequelize.addModels([Todos]);
      await sequelize.sync();
      return sequelize;
    },
  },
];
